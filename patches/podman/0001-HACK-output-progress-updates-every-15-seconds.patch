From 400965c93dc632c36104037c50811005596f3cd9 Mon Sep 17 00:00:00 2001
From: =?UTF-8?q?Martin=20Roukala=20=28n=C3=A9=20Peres=29?=
 <martin.roukala@mupuf.org>
Date: Wed, 13 Sep 2023 07:24:24 +0300
Subject: [PATCH] HACK: output progress updates every 15 seconds

When downloading big container images in boot2container over a serial
console, it is possible to go minutes without getting any update on the
terminal. This is problematic because it is indistinguishable from a
hung machine. This commit works around it by always pretending to be
running in a terminal, thus always generating the refresh lines.

However, if we left it at that, it would flood the serial console every
150ms, so we raise the refresh period to every 15s instead. This seems
like an acceptable refresh rate for humans and machines, while also
reducing the amount of lines in the overall output log.
---
 vendor/github.com/containers/image/v5/copy/copy.go    | 10 +---------
 vendor/github.com/vbauerster/mpb/v8/cwriter/writer.go | 10 +++++-----
 vendor/github.com/vbauerster/mpb/v8/progress.go       |  2 +-
 3 files changed, 7 insertions(+), 15 deletions(-)

diff --git a/vendor/github.com/containers/image/v5/copy/copy.go b/vendor/github.com/containers/image/v5/copy/copy.go
index ad1453fcb..f3a3f0642 100644
--- a/vendor/github.com/containers/image/v5/copy/copy.go
+++ b/vendor/github.com/containers/image/v5/copy/copy.go
@@ -223,14 +223,6 @@ func Image(ctx context.Context, policyContext *signature.PolicyContext, destRef,
 		}
 	}()
 
-	// If reportWriter is not a TTY (e.g., when piping to a file), do not
-	// print the progress bars to avoid long and hard to parse output.
-	// Instead use printCopyInfo() to print single line "Copying ..." messages.
-	progressOutput := reportWriter
-	if !isTTY(reportWriter) {
-		progressOutput = io.Discard
-	}
-
 	c := &copier{
 		policyContext: policyContext,
 		dest:          dest,
@@ -238,7 +230,7 @@ func Image(ctx context.Context, policyContext *signature.PolicyContext, destRef,
 		options:       options,
 
 		reportWriter:   reportWriter,
-		progressOutput: progressOutput,
+		progressOutput: reportWriter,
 
 		unparsedToplevel: image.UnparsedInstance(rawSource, nil),
 		// FIXME? The cache is used for sources and destinations equally, but we only have a SourceCtx and DestinationCtx.
diff --git a/vendor/github.com/vbauerster/mpb/v8/cwriter/writer.go b/vendor/github.com/vbauerster/mpb/v8/cwriter/writer.go
index 23a72d3ec..e6575456c 100644
--- a/vendor/github.com/vbauerster/mpb/v8/cwriter/writer.go
+++ b/vendor/github.com/vbauerster/mpb/v8/cwriter/writer.go
@@ -28,11 +28,11 @@ func New(out io.Writer) *Writer {
 	}
 	if f, ok := out.(*os.File); ok {
 		w.fd = int(f.Fd())
-		if IsTerminal(w.fd) {
-			w.terminal = true
-			w.termSize = func(fd int) (int, int, error) {
-				return GetSize(fd)
-			}
+
+		w.terminal = true
+		// HACK: Always pretend to be on a classic 80x24 console to work around serial-console-related issues
+		w.termSize = func(fd int) (int, int, error) {
+			return 80, 24, nil
 		}
 	}
 	bb := make([]byte, 16)
diff --git a/vendor/github.com/vbauerster/mpb/v8/progress.go b/vendor/github.com/vbauerster/mpb/v8/progress.go
index f275be3e5..066d7b489 100644
--- a/vendor/github.com/vbauerster/mpb/v8/progress.go
+++ b/vendor/github.com/vbauerster/mpb/v8/progress.go
@@ -15,7 +15,7 @@ import (
 )
 
 const (
-	defaultRefreshRate = 150 * time.Millisecond
+	defaultRefreshRate = 15 * time.Second
 )
 
 // DoneError represents an error when `*mpb.Progress` is done but its functionality is requested.
-- 
2.42.0

