QEMU_NO_NIC="-nic none"
QEMU_NIC="-netdev user,id=hostnet0 -device virtio-net-pci,netdev=hostnet0"
QEMU_DISK="-drive file=/disk.img,format=raw,if=virtio"
QEMU_TEST_CONTAINERS="-virtfs local,path=/test-containers/,mount_tag=test-containers,security_model=passthrough"
DISK_PATH=/disk.img

reset_disk() {
    rm "$DISK_PATH" 2> /dev/null
    fallocate -l 512M "$DISK_PATH"
}

run_test() {
    tmpfile=$(mktemp /tmp/qemu_exec.XXXXXX)

    cmdline="$QEMU \
-kernel $B2C_KERNEL -initrd $B2C_INITRD \
-nographic -m 512M -smp 4 $qemu_params \
-append 'console=ttyS0 $kernel_cmdline'"

    # TODO: ARM64: For some reason, I only get logs out when setting `earlycon`
    # TODO: ARM64: Performance when running in the test environment is 10x slower than the manual tests one

    # Add the EFI firmware, if needed
    [ -s "$EFI_PATH" ] && cmdline="$cmdline -drive if=pflash,format=raw,file=$EFI_PATH,readonly=on"

    echo -e "section_start:`date +%s`:b2c_kernel_boot[collapsed=true]\r\e[0K\033[0;36mStarting the VM\033[0m\n"

    eval "timeout 60 $cmdline" | tee "$tmpfile"

    stdout=$(cat "$tmpfile")
    rm "$tmpfile"
}
