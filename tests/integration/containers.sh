source ./tests/integration/base.sh

testDefaultContainerPipeline() {
    qemu_params="$QEMU_NIC $QEMU_DISK $QEMU_TEST_CONTAINERS"
    kernel_cmdline='b2c.filesystem="test-containers,src=test-containers,type=9p" \
        b2c.volume="test-containers,filesystem=test-containers" \
        b2c.load="test-containers/exit-container.tar" \
        b2c.run="-e EXIT_CODE=42 --pull=never localhost/exit-container:latest" \
        b2c.run="-e EXIT_CODE=43 --pull=never localhost/exit-container:latest" \
        b2c.run_post="-e EXIT_CODE=130 --pull=never localhost/exit-container:latest" \
        b2c.run_post="-e EXIT_CODE=129 --pull=never localhost/exit-container:latest" \
        b2c.reboot_cmd="poweroff -f"'
    reset_disk
    run_test

    # First container
    assertContains "$stdout" '-e B2C_PIPELINE_STATUS=0'
    assertContains "$stdout" '-e B2C_PIPELINE_FAILED_BY=""'
    assertContains "$stdout" '-e B2C_PIPELINE_PREV_CONTAINER=""'
    assertContains "$stdout" '-e B2C_PIPELINE_PREV_CONTAINER_EXIT_CODE='
    assertContains "$stdout" '-e B2C_PIPELINE_SHUTDOWN_MODE='
    assertContains "$stdout" "Exiting with code: 42"
    assertContains "$stdout" "The container exited with error code 42, continuing..."

    # Second container, executed despite the previous one failing
    assertContains "$stdout" '-e B2C_PIPELINE_STATUS=42'
    assertContains "$stdout" '-e B2C_PIPELINE_FAILED_BY="-e EXIT_CODE=42 --pull=never localhost/exit-container:latest"'
    assertContains "$stdout" '-e B2C_PIPELINE_PREV_CONTAINER="-e EXIT_CODE=42 --pull=never localhost/exit-container:latest"'
    assertContains "$stdout" '-e B2C_PIPELINE_PREV_CONTAINER_EXIT_CODE=42'
    assertContains "$stdout" "Exiting with code: 43"
    assertContains "$stdout" "The container exited with error code 43, continuing..."

    # First post-container, executed despite the pipeline failing
    assertContains "$stdout" '-e B2C_PIPELINE_STATUS=42'
    assertContains "$stdout" '-e B2C_PIPELINE_FAILED_BY="-e EXIT_CODE=42 --pull=never localhost/exit-container:latest"'
    assertContains "$stdout" '-e B2C_PIPELINE_PREV_CONTAINER="-e EXIT_CODE=43 --pull=never localhost/exit-container:latest"'
    assertContains "$stdout" '-e B2C_PIPELINE_PREV_CONTAINER_EXIT_CODE=43'
    assertContains "$stdout" "Exiting with code: 130"

    # Second post-container, executed despite the previous post container failing
    assertContains "$stdout" '-e B2C_PIPELINE_STATUS=42'
    assertContains "$stdout" '-e B2C_PIPELINE_FAILED_BY="-e EXIT_CODE=42 --pull=never localhost/exit-container:latest"'
    assertContains "$stdout" '-e B2C_PIPELINE_PREV_CONTAINER="-e EXIT_CODE=130 --pull=never localhost/exit-container:latest"'
    assertContains "$stdout" '-e B2C_PIPELINE_PREV_CONTAINER_EXIT_CODE=130'
    assertContains "$stdout" '-e B2C_PIPELINE_SHUTDOWN_MODE=poweroff'
    assertContains "$stdout" "Exiting with code: 129"

    # Check the overall pipeline status
    assertContains "$stdout" "Execution is over, pipeline status: 42"
    assertContains "$stdout" "Rebooting the computer"
}
suite_addTest testDefaultContainerPipeline

testContainerPipelineWithPipeFail() {
    qemu_params="$QEMU_NIC $QEMU_DISK $QEMU_TEST_CONTAINERS"
    kernel_cmdline='b2c.filesystem="test-containers,src=test-containers,type=9p" \
        b2c.volume="test-containers,filesystem=test-containers" \
        b2c.load="test-containers/exit-container.tar" \
        b2c.run="--pull=never localhost/exit-container:latest" \
        b2c.run="-e EXIT_CODE=42 --pull=never localhost/exit-container:latest" \
        b2c.run="-e EXIT_CODE=43 --pull=never localhost/exit-container:latest" \
        b2c.run_post="-e EXIT_CODE=44 --pull=never localhost/exit-container:latest" \
        b2c.run_post="-e EXIT_CODE=45 --pull=never localhost/exit-container:latest" \
        b2c.pipefail'
    reset_disk
    run_test

    # Successful container, so we can run the next one
    assertContains "$stdout" '-e B2C_PIPELINE_STATUS=0'
    assertContains "$stdout" '-e B2C_PIPELINE_FAILED_BY=""'
    assertContains "$stdout" '-e B2C_PIPELINE_PREV_CONTAINER=""'
    assertContains "$stdout" '-e B2C_PIPELINE_PREV_CONTAINER_EXIT_CODE='
    assertContains "$stdout" "Exiting with code: 0"
    assertContains "$stdout" "The container run successfully, load the next one!"

    # First failing container
    assertContains "$stdout" '-e B2C_PIPELINE_PREV_CONTAINER=""'
    assertContains "$stdout" '-e B2C_PIPELINE_PREV_CONTAINER_EXIT_CODE='
    assertContains "$stdout" "Exiting with code: 42"
    assertContains "$stdout" "The container exited with error code 42, aborting the pipeline..."

    # Second failing container (should not have been executed)
    assertNotContains "$stdout" "Exiting with code: 43"

    # First post-container, executed right after the first failing container
    assertContains "$stdout" '-e B2C_PIPELINE_STATUS=42'
    assertContains "$stdout" '-e B2C_PIPELINE_FAILED_BY="-e EXIT_CODE=42 --pull=never localhost/exit-container:latest"'
    assertContains "$stdout" '-e B2C_PIPELINE_PREV_CONTAINER="-e EXIT_CODE=42 --pull=never localhost/exit-container:latest"'
    assertContains "$stdout" '-e B2C_PIPELINE_PREV_CONTAINER_EXIT_CODE=42'
    assertContains "$stdout" "Exiting with code: 44"

    # Second post-container, executed despite the previous post container failing
    assertContains "$stdout" '-e B2C_PIPELINE_STATUS=42'
    assertContains "$stdout" '-e B2C_PIPELINE_FAILED_BY="-e EXIT_CODE=42 --pull=never localhost/exit-container:latest"'
    assertContains "$stdout" '-e B2C_PIPELINE_PREV_CONTAINER="-e EXIT_CODE=44 --pull=never localhost/exit-container:latest"'
    assertContains "$stdout" '-e B2C_PIPELINE_PREV_CONTAINER_EXIT_CODE=44'
    assertContains "$stdout" "Exiting with code: 45"

    # Check the overall pipeline status
    assertContains "$stdout" "Execution is over, pipeline status: 42"
    assertContains "$stdout" "It's now safe to turn off your computer"
}
suite_addTest testContainerPipelineWithPipeFail

# TODO: Add `b2c.run_service` tests
